import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
      this.peliculasEnCines = [{
        titulo: 'Spider Man Home comming',
        fechaLanzamiento: new Date(),
        precio: 224,
        poster: 'https://m.media-amazon.com/images/I/81GdgigtG+L._AC_SY679_.jpg'
      },
      {
        titulo: 'Spier Man 2',
        fechaLanzamiento: new Date(),
        precio: 200,
        poster: 'https://m.media-amazon.com/images/I/71BR8hBql9L._AC_SY741_.jpg'
      },
      {
        titulo: 'Spider Man 3',
        fechaLanzamiento: new Date(),
        precio: 100,
        poster: 'https://m.media-amazon.com/images/I/71Ge7Hqy+8L._AC_SY679_.jpg'
      }];
      
      this.peliculasProximos = [{
        titulo: 'Rocky I',
        fechaLanzamiento: new Date(),
        precio: 224,
        poster: 'https://m.media-amazon.com/images/I/61SZsZYtwcL._AC_SL1001_.jpg'
      },
      {
        titulo: 'Rocky II',
        fechaLanzamiento: new Date(),
        precio: 200,
        poster: 'https://m.media-amazon.com/images/I/91gZS1i7jCL._AC_SY741_.jpg'
      },
      {
        titulo: 'Rocky III',
        fechaLanzamiento: new Date(),
        precio: 100,
        poster: 'https://m.media-amazon.com/images/I/91xUYVWBS0L._AC_SY741_.jpg'
      },
      {
        titulo: 'Rocky IV',
        fechaLanzamiento: new Date(),
        precio: 100,
        poster: 'https://m.media-amazon.com/images/I/91feZ8nRyQL._AC_SY741_.jpg'
      },
      {
        titulo: 'Rocky V',
        fechaLanzamiento: new Date(),
        precio: 200.4,
        poster: 'https://m.media-amazon.com/images/I/41-iQRytc3L._AC_.jpg'
      },
      {
        titulo: 'Rocky Balboa',
        fechaLanzamiento: new Date(),
        precio: 300.8,
        poster: 'https://m.media-amazon.com/images/I/41llSHHkSUL._AC_.jpg'
      }];
    };
    
  title = 'front-end';
  peliculasEnCines;
  peliculasProximos;

 

}
