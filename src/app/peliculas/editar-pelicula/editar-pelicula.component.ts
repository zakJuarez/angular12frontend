import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PeliculaDTO, PeliculaCreacionDTO } from '../pelicula';

@Component({
  selector: 'app-editar-pelicula',
  templateUrl: './editar-pelicula.component.html',
  styleUrls: ['./editar-pelicula.component.css']
})
export class EditarPeliculaComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute) { }

  modelo: PeliculaDTO = { 
      titulo: 'Spiderman', 
      trailer: ' abc.url.mas', enCines: true, resumen: '**texto en negritas**', 
      fechaLanzamiento: new Date(), 
      poster: 'https://m.media-amazon.com/images/I/51OzUVuru0L._AC_.jpg'
  };

  ngOnInit(): void {
  }

  guardarCambios(pelicula: PeliculaCreacionDTO){
    console.log(pelicula);
  }

}
