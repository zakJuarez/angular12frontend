import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PeliculaCreacionDTO, PeliculaDTO } from '../pelicula';
import { MultipleSelectorModule } from '../../utilidades/selector-multiple/MultipleSelectorModule';

@Component({
  selector: 'app-formulario-pelicula',
  templateUrl: './formulario-pelicula.component.html',
  styleUrls: ['./formulario-pelicula.component.css']
})
export class FormularioPeliculaComponent implements OnInit {

  @Output()
  OnSubmit: EventEmitter<PeliculaCreacionDTO> = new EventEmitter<PeliculaCreacionDTO>();

  generosNoSeleccionados: MultipleSelectorModule[] = [
    { llave: 1, valor: 'Drama' },
    { llave: 2, valor: 'Acción' },
    { llave: 3, valor: 'Thriller' },
    { llave: 4, valor: 'Comedia' },
  ];

  cinesNoSeleccionados: MultipleSelectorModule[] = [
    { llave: 1, valor: 'Cinepolis Patriotismo' },
    { llave: 2, valor: 'Cinemex Reforma' },
    { llave: 3, valor: 'Cinemex WTC' },
    { llave: 4, valor: 'Plaza sendero' },
    { llave: 5, valor: 'Cinepolis Patio Texcoco' }
  ];

  generosSeleccionados: MultipleSelectorModule[] = [];
  cinesSeleccionados: MultipleSelectorModule[] = [];

  constructor(private formBuilder: FormBuilder) { }

  form: FormGroup;

  @Input()
  modelo: PeliculaDTO;

  ngOnInit(): void {
    this.form= this.formBuilder.group({
      titulo: ['', {validators: [Validators.required]}],
      resumen: '',
      enCines: false,
      trailer:'',
      fechaLanzamiento: '',
      poster: '',
      generosId: [],
      cinesId: []
    })

    if(this.modelo !== undefined){
      this.form.patchValue(this.modelo);
    }
  }

  guardarCambios(){
    const generosIds = this.generosSeleccionados.map(val => val.llave);
    const cinesIds = this.cinesSeleccionados.map(val => val.llave);
    this.form.get('generosId').setValue(generosIds);
    this.form.get('cinesId').setValue(cinesIds);
    this.OnSubmit.emit(this.form.value);
  }

  archivoSeleccionado(archivo: File){
    this.form.get('poster').setValue(archivo);
  }

  changeMarkdown(texto){
    this.form.get('resumen').setValue(texto);
  }

}

