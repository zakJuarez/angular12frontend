import { calcPossibleSecurityContexts } from '@angular/compiler/src/template_parser/binding_parser';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { observable } from 'rxjs';

@Component({
  selector: 'app-filtro-peliculas',
  templateUrl: './filtro-peliculas.component.html',
  styleUrls: ['./filtro-peliculas.component.css']
})
export class FiltroPeliculasComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private location: Location, private activatedRoute: ActivatedRoute) { }

  form: FormGroup;

  generos = [{id:1, nombre: 'Drama'},{id:2, nombre: 'Comedia'},{id:3, nombre: 'Thriller'},{id:4, nombre: 'Acción'}];

  peliculasOriginal = [
    {
      titulo: 'Spider Man Home comming',
      fechaLanzamiento: new Date(),
      generoId: [1,3],
      enCines: false,
      proximosEstrenos: true,
      poster: 'https://m.media-amazon.com/images/I/81GdgigtG+L._AC_SY679_.jpg'
    },
    {
      titulo: 'Spider Man 2',
      fechaLanzamiento: new Date(),
      generoId: [2,3],
      enCines: true,
      proximosEstrenos: false,
      poster: 'https://m.media-amazon.com/images/I/71BR8hBql9L._AC_SY741_.jpg'
    },
    {
      titulo: 'Spider Man 3',
      fechaLanzamiento: new Date(),
      generoId: [1,3],
      enCines: false,
      proximosEstrenos: true,
      poster: 'https://m.media-amazon.com/images/I/71Ge7Hqy+8L._AC_SY679_.jpg'
    },
    {
      titulo: 'Rocky I',
      fechaLanzamiento: new Date(),
      generoId: [1,2],
      enCines: true,
      proximosEstrenos: false,
      poster: 'https://m.media-amazon.com/images/I/61SZsZYtwcL._AC_SL1001_.jpg'
    },
    {
      titulo: 'Rocky II',
      fechaLanzamiento: new Date(),
      generoId: [1,2],
      enCines: false,
      proximosEstrenos: true,
      poster: 'https://m.media-amazon.com/images/I/91gZS1i7jCL._AC_SY741_.jpg'
    },
    {
      titulo: 'Rocky III',
      fechaLanzamiento: new Date(),
      generoId: [1],
      enCines: true,
      proximosEstrenos: false,
      poster: 'https://m.media-amazon.com/images/I/91xUYVWBS0L._AC_SY741_.jpg'
    },
    {
      titulo: 'Rocky IV',
      fechaLanzamiento: new Date(),
      generoId: [1,2,3],
      enCines: false,
      proximosEstrenos: true,
      poster: 'https://m.media-amazon.com/images/I/91feZ8nRyQL._AC_SY741_.jpg'
    },
    {
      titulo: 'Rocky V',
      fechaLanzamiento: new Date(),
      generoId: [3],
      enCines: true,
      proximosEstrenos: false,
      poster: 'https://m.media-amazon.com/images/I/41-iQRytc3L._AC_.jpg'
    },
    {
      titulo: 'Rocky Balboa',
      fechaLanzamiento: new Date(),
      generoId: [2],
      enCines: false,
      proximosEstrenos: true,
      poster: 'https://m.media-amazon.com/images/I/41llSHHkSUL._AC_.jpg'
    }
  ];

  peliculas = [
    {
      titulo: 'Spider Man Home comming',
      fechaLanzamiento: new Date(),
      generoId: [1,3],
      enCines: false,
      proximosEstrenos: true,
      poster: 'https://m.media-amazon.com/images/I/81GdgigtG+L._AC_SY679_.jpg'
    },
    {
      titulo: 'Spider Man 2',
      fechaLanzamiento: new Date(),
      generoId: [2,3],
      enCines: true,
      proximosEstrenos: false,
      poster: 'https://m.media-amazon.com/images/I/71BR8hBql9L._AC_SY741_.jpg'
    },
    {
      titulo: 'Spider Man 3',
      fechaLanzamiento: new Date(),
      generoId: [1,3],
      enCines: false,
      proximosEstrenos: true,
      poster: 'https://m.media-amazon.com/images/I/71Ge7Hqy+8L._AC_SY679_.jpg'
    },
    {
      titulo: 'Rocky I',
      fechaLanzamiento: new Date(),
      generoId: [1,2],
      enCines: true,
      proximosEstrenos: false,
      poster: 'https://m.media-amazon.com/images/I/61SZsZYtwcL._AC_SL1001_.jpg'
    },
    {
      titulo: 'Rocky II',
      fechaLanzamiento: new Date(),
      generoId: [1,2],
      enCines: false,
      proximosEstrenos: true,
      poster: 'https://m.media-amazon.com/images/I/91gZS1i7jCL._AC_SY741_.jpg'
    },
    {
      titulo: 'Rocky III',
      fechaLanzamiento: new Date(),
      generoId: [1],
      enCines: true,
      proximosEstrenos: false,
      poster: 'https://m.media-amazon.com/images/I/91xUYVWBS0L._AC_SY741_.jpg'
    },
    {
      titulo: 'Rocky IV',
      fechaLanzamiento: new Date(),
      generoId: [1,2,3],
      enCines: false,
      proximosEstrenos: true,
      poster: 'https://m.media-amazon.com/images/I/91feZ8nRyQL._AC_SY741_.jpg'
    },
    {
      titulo: 'Rocky V',
      fechaLanzamiento: new Date(),
      generoId: [3],
      enCines: true,
      proximosEstrenos: false,
      poster: 'https://m.media-amazon.com/images/I/41-iQRytc3L._AC_.jpg'
    },
    {
      titulo: 'Rocky Balboa',
      fechaLanzamiento: new Date(),
      generoId: [2],
      enCines: false,
      proximosEstrenos: true,
      poster: 'https://m.media-amazon.com/images/I/41llSHHkSUL._AC_.jpg'
    }
  ];

  formularioOriginal = {
    titulo: '',
    generoId: 0,
    proximosEstrenos: false,
    enCines: false
  };

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      titulo: '',
      generoId: 0,
      proximosEstrenos: false,
      enCines: false
    });
    this.leerValoresURL();
    this.buscarPeliculas(this.form.value);

    this.form.valueChanges.subscribe(
      valores => {
        this.peliculas = this.peliculasOriginal;
        this.buscarPeliculas(valores);
        this.escribirParametrosBusquedaURL();
      })
  }

  private leerValoresURL(){
    this.activatedRoute.queryParams.subscribe((params) => {
      var objeto : any = {  };
      if(params.titulo){
        objeto.titulo = params.titulo;
      }
      if(params.generoId){
        objeto.generoId = Number(params.generoId);
      }
      if(params.proximosEstrenos){
        objeto.proximosEstrenos = params.proximosEstrenos;
      }
      if(params.enCines){
        objeto.enCines = params.enCines;
      }

      this.form.patchValue(objeto);
    })
  }

  private escribirParametrosBusquedaURL() {
    var queryString = [];
    var valoresFormulario = this.form.value;
    if(valoresFormulario.titulo){
      queryString.push(`titulo=${valoresFormulario.titulo}`);
    }
    if(valoresFormulario.generoId != '0'){
      queryString.push(`generoId=${valoresFormulario.generoId}`);
    }
    if(valoresFormulario.proximosEstrenos){
      queryString.push(`proximosEstrenos=${valoresFormulario.proximosEstrenos}`);
    }
    if(valoresFormulario.enCines){
      queryString.push(`enCines=${valoresFormulario.enCines}`);
    }
    this.location.replaceState('peliculas/buscar',queryString.join('&'));
  }

  buscarPeliculas(valores: any){
    if(valores.titulo){
     this.peliculas = this.peliculas.filter(pelicula => pelicula.titulo.indexOf(valores.titulo) !== -1);
    }

    if(valores.generoId !== 0){
      this.peliculas = this.peliculas.filter(pelicula => pelicula.generoId.indexOf(valores.generoId) !== -1);
    }

    if(valores.proximosEstrenos){
      this.peliculas = this.peliculas.filter(pelicula => pelicula.proximosEstrenos);
    }

    if(valores.enCines){
      this.peliculas = this.peliculas.filter(pelicula => pelicula.enCines);
    }
  }

  limpiar(){
    this.form.patchValue(this.formularioOriginal);
  }

}
