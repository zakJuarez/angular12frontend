import { Component, Input, OnInit } from '@angular/core';
import { ListadoPeliculasComponent } from '../../peliculas/listado-peliculas/listado-peliculas.component';

@Component({
  selector: 'app-listado-generico',
  templateUrl: './listado-generico.component.html',
  styleUrls: ['./listado-generico.component.css']
})
export class ListadoGenericoComponent implements OnInit {

  @Input()
  listado;
  constructor() { }

  ngOnInit(): void {
  }

}
